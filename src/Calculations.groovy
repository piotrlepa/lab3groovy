class Calculations {
    static Point2D positionGeometricCenter(Point2D[] points) {
        int size = points.size()
        final def x = points.sum { p -> p.x } / size
        final def y = points.sum { p -> p.y } / size
        return new Point2D(x, y)
    }

    static Point2D positionCenterOfMass(MaterialPoint2D[] points) {
        final def x = points.sum { p -> p.x * p.mass }
        final def y = points.sum { p -> p.y * p.mass }
        final def mass = points.sum { p -> p.mass }
        return new MaterialPoint2D(x / mass, y / mass, mass)
    }
}
