class MaterialPoint2D extends Point2D {
    private final int mass

    MaterialPoint2D(double x, double y, int mass) {
        super(x, y)
        this.mass = mass
    }

    int getMass() {
        return mass
    }

    @Override
    String toString() {
        return super.toString() + "MaterialPoint2D{" +
                "mass=" + mass +
                '}'
    }
}